#!/bin/sh

DIALOG_HEIGHT=256
TIMEOUT_SECONDS=60

TEXT="The chosen action will executed in $TIMEOUT_SECONDS seconds."
OK_TEXT="Do it now!"

ER_UNDEFINED_ACTION='Undefined shutdown option: '

ISSUES_URL='https://gitlab.com/vasilyb/shuter/issues'

FAIL_REPORT="

Operation has been canceled.
You may post a bug report at the next web page
$ISSUES_URL"

TITLE='--title=shuter'

LIST_HEADER='What would you like to do?'

ITEM_POWEROFF='Power Off'
ITEM_REBOOT='Reboot'

VALUE_POWEROFF='p'
VALUE_REBOOT='r'

VALUES_PATTERN="[${VALUE_POWEROFF}${VALUE_REBOOT}]"

LIST_ITEMS=\
"True
$VALUE_POWEROFF
$ITEM_POWEROFF
False
$VALUE_REBOOT
$ITEM_REBOOT"

ACTION=`echo "$LIST_ITEMS" | zenity --list\
	$TITLE\
	--text="$TEXT"\
	--column=''\
	--column=''\
	--column="$LIST_HEADER"\
	--hide-column=2\
	--radiolist\
	--timeout=$TIMEOUT_SECONDS\
	--height=$DIALOG_HEIGHT\
	--ok-label="$OK_TEXT"`

if [ -z "$ACTION" ]
	then exit
fi

if [ `expr "$ACTION" : $VALUES_PATTERN` -eq 0 ]
	then zenity --error\
		$TITLE\
		--text="${ER_UNDEFINED_ACTION}${ACTION}${FAIL_REPORT}" ; exit
fi

shutdown -$ACTION now

